package by.ilokhin.elilink.tasks.testSuite;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import by.ilokhin.elilink.tasks.BaseTest;
import by.ilokhin.elilink.tasks.helpers.MethodAddLettersToList;
import by.ilokhin.elilink.tasks.pages.pagesTaskTwo.MainPage;
import by.ilokhin.elilink.tasks.pages.pagesTaskTwo.StartPage;
import by.ilokhin.elilink.tasks.pages.pagesTaskTwo.WriteLetterPage;



public class TaskTwo extends BaseTest {
	
	private static final Logger log = Logger.getLogger(TaskTwo.class.getName());

	
	@Test
	public void taskTwo() throws Exception {
		
		log.info("Start test - (task2)");
		driver.get("https://www.google.com/intl/ru/gmail/about/");
		final StartPage startPage = PageFactory.initElements(driver, StartPage.class);
		startPage.enter("uu9632566@gmail.com", "rtyu123HE456abc");
		final MainPage mainPage = PageFactory.initElements(driver, MainPage.class);
		mainPage.checkDir("in:sent ", "in:spam ", "Несортированные");
		final MainPage countLetters = PageFactory.initElements(driver, MainPage.class);
		final int count = countLetters.getResultsCount();
		log.info("All letters in inbox = " + count + " pieces - (task2)");
		mainPage.searchLetters("a");
		final int countAfterSearch = countLetters.getResultsCountAfterSearch();
		log.info("Letters with 'a' = " + countAfterSearch + " pieces - (task2)");
		MethodAddLettersToList.addLettersToList(driver);
		mainPage.pressWrite();
		final WriteLetterPage writeLetter = PageFactory.initElements(driver, WriteLetterPage.class);
		writeLetter.writeLetter("myzhdashizolotar@gmail.com", "new letter", "Hello my friend! I'm sending for you a new letter!");
		log.info("Letter wrote and sent - (task2)");
		mainPage.logout();
		log.info("Exit from account - (task2)");
		log.info("TEST PASSED - (task2)");
		
	}

}
