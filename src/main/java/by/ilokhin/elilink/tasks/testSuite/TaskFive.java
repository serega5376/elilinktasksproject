package by.ilokhin.elilink.tasks.testSuite;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TaskFive {
	
	private static final Logger log = Logger.getLogger(TaskFive.class.getName());

	@Test
	public void taskFive() throws Exception {
		log.info("Start test - (task5)");
		final String content = executeGet("https://www.cnn.com/");
//		log.info(content);
		Pattern p = Pattern.compile("(?i)trump");
		Matcher m = p.matcher(content);
		int i = 0;
		while (m.find()) {
			i++;
		}
		log.info(i);
		log.info("TEST PASSED - (task5)");
	}

	private String executeGet(String url) throws IOException {
		HttpGet get = new HttpGet(url);

		final CloseableHttpResponse response = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build().execute(get);
		if (200 != response.getStatusLine().getStatusCode()) {
			log.info(response.getStatusLine());
			throw new RuntimeException("Incorrect request");
		}
		return EntityUtils.toString(response.getEntity());
	}

}
