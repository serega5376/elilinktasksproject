package by.ilokhin.elilink.tasks.testSuite;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import by.ilokhin.elilink.tasks.BaseTest;
import by.ilokhin.elilink.tasks.pages.pagesTaskThree.AddPayDataPage;
import by.ilokhin.elilink.tasks.pages.pagesTaskThree.AddPersonDataPage;
import by.ilokhin.elilink.tasks.pages.pagesTaskThree.ChooseTicketPage;
import by.ilokhin.elilink.tasks.pages.pagesTaskThree.MainPage;



public class TaskThree extends BaseTest {
	
	private static final Logger log = Logger.getLogger(TaskThree.class.getName());

	
	@Test
	public void taskThree() throws Exception {
		
		log.info("Start test - (task3)");
		driver.get("https://www.delta.com/");
		final MainPage startPage = PageFactory.initElements(driver, MainPage.class);
		startPage.addFlightData("JFK", "SVO");
		log.info("Open home page | Booking widget- (task3)");
		final ChooseTicketPage chooseTickets = PageFactory.initElements(driver, ChooseTicketPage.class);
		chooseTickets.chooseTicketAndSumm();
		log.info("Tickets selection page | Trip Summary page - (task3)");
		final AddPersonDataPage infoPassengerPage = PageFactory.initElements(driver, AddPersonDataPage.class);
		infoPassengerPage.addPersonData("Penkin", "Boris", "1234567890", "qa@test.com");
		log.info("Passenger Info page | Trip Extras page - (task3)");
		final AddPayDataPage chooseTicketAndSumm = PageFactory.initElements(driver, AddPayDataPage.class);
		chooseTicketAndSumm.chooseTicketAndSumm("123456789012345", "Penkin Boris", "Street", "New York", "123");
		log.info("Credit Card Info page - (task3)");
		log.info("TEST PASSED - (task3)");
	}
	
}
