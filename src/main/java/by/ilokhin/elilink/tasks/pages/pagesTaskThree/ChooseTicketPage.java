package by.ilokhin.elilink.tasks.pages.pagesTaskThree;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import by.ilokhin.elilink.tasks.pages.BasePage;

public class ChooseTicketPage extends BasePage {
	
	@FindBy(xpath = ".//*[@id='maincontent']/div/div[2]/div/div[6]/table/tbody/tr/td/div[1]/table/tbody/tr[3]/td[2]/div/a")
	private WebElement chooseTicket;
	
	@FindBy(xpath = ".//*[@id='tripSummarySubmitBtn']")
	private WebElement buttonSummSubmit;
	
	@FindBy(xpath = "//button[text()='Change Flight']")
	private WebElement buttonChangeFlight;
	
	public ChooseTicketPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	public ChooseTicketPage chooseTicketAndSumm() throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(webDriver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(chooseTicket));
		chooseTicket.click();
		wait.until(ExpectedConditions.elementToBeClickable(buttonChangeFlight));
		chooseTicket.click();
		wait.until(ExpectedConditions.elementToBeClickable(buttonSummSubmit));
		buttonSummSubmit.click();

		return new ChooseTicketPage(webDriver);
	}

}
