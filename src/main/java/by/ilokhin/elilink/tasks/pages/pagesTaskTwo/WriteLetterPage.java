package by.ilokhin.elilink.tasks.pages.pagesTaskTwo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import by.ilokhin.elilink.tasks.pages.BasePage;

public class WriteLetterPage extends BasePage {
	
	@FindBy(xpath = "//textarea[contains(@aria-label,'Кому')]")
	private WebElement fieldTo;
	
	@FindBy(xpath = "//input[contains(@placeholder,'Тема')]")
	private WebElement fieldTopic;
	
	@FindBy(xpath = "//div[contains(@aria-label,'Тело письма')]")
	private WebElement fieldBodyLetter;
	
	@FindBy(xpath = "//div[text()='Отправить']")
	private WebElement buttonSend;
	
	@FindBy(xpath = "html/body/div[7]/div[3]/div/div[1]/div[5]/div[1]/div/div[3]/div/div/div[2]")
	private WebElement dispatchMessage;
	
	public WriteLetterPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	public WriteLetterPage writeLetter(String sendLetterTo, String topicLetter, String textLetter) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(webDriver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(fieldTo));
		fieldTo.sendKeys(sendLetterTo);
		fieldTopic.sendKeys(topicLetter);
		fieldBodyLetter.sendKeys(textLetter);
		buttonSend.click();
		wait.until(ExpectedConditions.elementToBeClickable(dispatchMessage));

		return new WriteLetterPage(webDriver);
	}

}
