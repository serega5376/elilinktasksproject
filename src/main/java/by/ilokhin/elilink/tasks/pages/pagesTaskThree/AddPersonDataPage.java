package by.ilokhin.elilink.tasks.pages.pagesTaskThree;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import by.ilokhin.elilink.tasks.pages.BasePage;

public class AddPersonDataPage extends BasePage {
	
	@FindBy(xpath = ".//*[@id='passengerDetails0']/div[2]/div/h3")
	private WebElement headerWait;
	
	@FindBy(xpath = ".//*[@id='prefix0-button']/span[1]")
	private WebElement buttonPrefixMr;
	
	@FindBy(xpath = "//li[text()='Mr']")
	private WebElement prefixMr;
	
	@FindBy(xpath = ".//*[@id='firstName0']")
	private WebElement fieldFirstName;
	
	@FindBy(xpath = ".//*[@id='lastName0']")
	private WebElement fieldLastName;
	
	@FindBy(xpath = ".//*[@id='gender0-button']/span[2]")
	private WebElement buttonPrefixMale;
	
	@FindBy(xpath = "//li[text()='Male']")
	private WebElement prefixMale;
	
	@FindBy(xpath = ".//*[@id='month0-button']/span[1]")
	private WebElement buttonPrefixMonth;
	
	@FindBy(xpath = "//li[text()='January']")
	private WebElement preefixMonth;
	
	@FindBy(xpath = ".//*[@id='day0-button']/span[1]")
	private WebElement buttonPrefixDay;
	
	@FindBy(xpath = "//li[text()='1']")
	private WebElement preefixDay;
	
	@FindBy(xpath = ".//*[@id='year0-button']/span[1]")
	private WebElement buttonPrefixYear;
	
	@FindBy(xpath = "html/body/div[16]/ul/li[19]")
	private WebElement preefixYear;
	
	@FindBy(xpath = ".//*[@id='emgcFirstName_0']")
	private WebElement fieldFirstNameIns;
	
	@FindBy(xpath = ".//*[@id='emgcLastName_0']")
	private WebElement fieldLastNameIns;
	
	@FindBy(xpath = ".//*[@id='emgcPhoneNumber_0']")
	private WebElement emgcPhoneNumber;
	
	@FindBy(xpath = ".//*[@id='deviceType-button']/span[1]")
	private WebElement buttonDeviceType;
	
	@FindBy(xpath = "//li[text()='Business']")
	private WebElement personData;
	
	@FindBy(xpath = ".//*[@id='telephoneNumber0']")
	private WebElement telephoneNumber;
	
	@FindBy(xpath = ".//*[@id='email']")
	private WebElement emaiL;
	
	@FindBy(xpath = ".//*[@id='reEmail']")
	private WebElement reEmail;
	
	@FindBy(xpath = ".//*[@id='paxReviewPurchaseBtn']")
	private WebElement paxReviewPurchaseBtn;
	
	public AddPersonDataPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	public AddPersonDataPage addPersonData(String firstName, String lastName, String phoneN, String email)
			throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(webDriver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(headerWait));
		buttonPrefixMr.click();
		WebElement inputMr = prefixMr;
		new Actions(webDriver).moveToElement(inputMr).click().perform();
		fieldFirstName.sendKeys(firstName);
		fieldLastName.sendKeys(lastName);
		buttonPrefixMale.click();
		WebElement input = prefixMale;
		new Actions(webDriver).moveToElement(input).click().perform();
		buttonPrefixMonth.click();
		WebElement inputMonth = preefixMonth;
		new Actions(webDriver).moveToElement(inputMonth).click().perform();
		buttonPrefixDay.click();
		WebElement inputDay = preefixDay;
		new Actions(webDriver).moveToElement(inputDay).click().perform();
		buttonPrefixYear.click();
		buttonPrefixYear.sendKeys("1");
		WebElement inputYear = preefixYear;
		new Actions(webDriver).moveToElement(inputYear).click().perform();
		fieldFirstNameIns.clear();
		fieldFirstNameIns.sendKeys(firstName);
		fieldLastNameIns.clear();
		fieldLastNameIns.sendKeys(lastName);
		emgcPhoneNumber.clear();
		emgcPhoneNumber.sendKeys(phoneN);
		buttonDeviceType.click();
		wait.until(ExpectedConditions.elementToBeClickable(personData));
		personData.click();
		telephoneNumber.sendKeys(phoneN);
		emaiL.sendKeys(email);
		reEmail.sendKeys(email);
		paxReviewPurchaseBtn.click();

		return new AddPersonDataPage(webDriver);
	}

}
