package by.ilokhin.elilink.tasks.pages.pagesTaskThree;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import by.ilokhin.elilink.tasks.pages.BasePage;

public class MainPage extends BasePage {
	
	@FindBy(css = "#originCity")
	private WebElement originCity;
	
	@FindBy(xpath = "//a[text()='New York-Kennedy, NY, US (JFK)']")
	private WebElement originPlaceCity;
	
	
	@FindBy(css = "#destinationCity")
	private WebElement destinationCity;
	
	@FindBy(xpath = "//a[text()='Moscow, Russia, RU (SVO)']")
	private WebElement airPlacebutton;
	
	@FindBy(xpath = ".//*[@id='depDateCalIcon']")
	private WebElement depDateCalIcon;
	
	@FindBy(xpath = ".//*[@id='ui-datepicker-div']/div[3]/table/tbody/tr[5]/td[1]/a")
	private WebElement depDate;
	
	@FindBy(xpath = ".//*[@id='retDateCalIcon']")
	private WebElement retDateCalIcon;
	
	@FindBy(xpath = ".//*[@id='ui-datepicker-div']/div[3]/table/tbody/tr[5]/td[4]/a")
	private WebElement retDate;
	
	@FindBy(xpath = ".//*[@id='exactDaysBtn']/span")
	private WebElement exactDaysBtn;
	
	@FindBy(xpath = ".//*[@id='cashBtn']/span")
	private WebElement cashBtn;
	
	@FindBy(xpath = ".//*[@id='findFlightsSubmit']")
	private WebElement findFlightsSubmit;
	
	public MainPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	public MainPage addFlightData(String from, String to) throws InterruptedException {
//		Thread.sleep(2000);
		WebDriverWait wait = new WebDriverWait(webDriver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(originCity));
		originCity.clear();
		originCity.sendKeys(from);
		wait.until(ExpectedConditions.elementToBeClickable(originPlaceCity));
		originPlaceCity.click();
		destinationCity.clear();
		destinationCity.sendKeys(to);
		wait.until(ExpectedConditions.elementToBeClickable(airPlacebutton));
		airPlacebutton.click();
		depDateCalIcon.click();
		wait.until(ExpectedConditions.elementToBeClickable(depDate));
		depDate.click();
		retDateCalIcon.click();
		wait.until(ExpectedConditions.elementToBeClickable(retDate));
		retDate.click();
		exactDaysBtn.click();
		cashBtn.click();
		findFlightsSubmit.click();

		return new MainPage(webDriver);
	}

}
