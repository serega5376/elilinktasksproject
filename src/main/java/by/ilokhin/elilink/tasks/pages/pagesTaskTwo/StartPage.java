package by.ilokhin.elilink.tasks.pages.pagesTaskTwo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import by.ilokhin.elilink.tasks.pages.BasePage;

public class StartPage extends BasePage {

	@FindBy(linkText = "Войти")
	private WebElement buttonEnter;

	@FindBy(id = "identifierId")
	private WebElement idField;
	
	@FindBy(xpath = ".//*[@id='identifierNext']/content/span")
	private WebElement buttonIdNext;
	
	@FindBy(name = "password")
	private WebElement passField;
	
	@FindBy(xpath = ".//*[@id='passwordNext']/content/span")
	private WebElement buttonPassNext;

	


	public StartPage(WebDriver webDriver) {
		super(webDriver);
	}

	public StartPage enter(String idString, String passString) throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(webDriver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(buttonEnter));
		buttonEnter.click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(idField));
		idField.sendKeys(idString);
		buttonIdNext.click();
		wait.until(ExpectedConditions.elementToBeClickable(passField));
		passField.sendKeys(passString);
		buttonPassNext.click();

		return new StartPage (webDriver);
	}
}