package by.ilokhin.elilink.tasks.pages.pagesTaskOne;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import by.ilokhin.elilink.tasks.pages.BasePage;

public class SearchPage extends BasePage {

	private static final By RESULTS_LOCATOR = By.xpath("//li[@class='b-results__li']");

	public SearchPage(WebDriver webDriver) {
		super(webDriver);
	}

	public int getResultsCount() {
		return this.webDriver.findElements(RESULTS_LOCATOR).size();
	}
}
