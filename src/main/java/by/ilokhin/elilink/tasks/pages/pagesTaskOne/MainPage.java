package by.ilokhin.elilink.tasks.pages.pagesTaskOne;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import by.ilokhin.elilink.tasks.pages.BasePage;

public class MainPage extends BasePage {

	@FindBy(xpath = "//input[@id='search_from_str']")
	private WebElement searchInput;

	@FindBy(xpath = "//input[@name='search']")
	private WebElement searchButton;
	


	public MainPage(WebDriver webDriver) {
		super(webDriver);
	}

	public SearchPage search(String searchString) {
		
		searchInput.sendKeys(searchString);
		searchButton.click();
		

		return new SearchPage(webDriver);
	}
}
