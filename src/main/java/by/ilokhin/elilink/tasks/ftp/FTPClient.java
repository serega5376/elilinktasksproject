package by.ilokhin.elilink.tasks.ftp;

import org.apache.log4j.Logger;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class FTPClient {

	private static final Logger log = Logger.getLogger(FTPClient.class.getName());

	private Socket socket = null;
	private BufferedReader reader = null;
	private BufferedWriter writer = null;
	private static boolean DEBUG = true;

	/**
	 * Connects to the default port of an FTP server and logs in as
	 * anonymous/anonymous.
	 */
	public synchronized void connect(String host) throws IOException {
		connect(host, 21);
	}

	/**
	 * Connects to an FTP server and logs in as anonymous/anonymous.
	 */
	public synchronized void connect(String host, int port) throws IOException {
		connect(host, port, "anonymous", "anonymous");
	}

	/**
	 * Connects to an FTP server and logs in with the supplied username and
	 * password.
	 */
	public synchronized void connect(String host, int port, String user, String pass) throws IOException {
		if (socket != null) {
			throw new IOException("SimpleFTP is already connected. Disconnect first.");
		}
		socket = new Socket(host, port);
		reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

		String response = readLine();
		if (!response.startsWith("220 ")) {
			throw new IOException("SimpleFTP received an unknown response when connecting to the FTP server: " + response);
		}

		sendLine("USER " + user);

		response = readLine();
		if (!response.startsWith("331 ")) {
			throw new IOException("SimpleFTP received an unknown response after sending the user: " + response);
		}

		sendLine("PASS " + pass);

		response = readLine();
		if (!response.startsWith("230 ")) {
			throw new IOException("SimpleFTP was unable to log in with the supplied password: " + response);
		}

		// Now logged in.
	}

	/**
	 * Disconnects from the FTP server.
	 */
	public synchronized void disconnect() throws IOException {
		try {
			sendLine("QUIT");
		} finally {
			socket = null;
		}
	}

	/**
	 * Returns the working directory of the FTP server it is connected to.
	 */
	public synchronized String pwd() throws IOException {
		sendLine("PWD");
		String dir = null;
		String response = readLine();
		if (response.startsWith("257 ")) {
			int firstQuote = response.indexOf('\"');
			int secondQuote = response.indexOf('\"', firstQuote + 1);
			if (secondQuote > 0) {
				dir = response.substring(firstQuote + 1, secondQuote);
			}
		}
		return dir;
	}

	/**
	 * Returns the working directory of the FTP server it is connected to.
	 */
	public synchronized List<String> list() throws IOException {

		sendLine("PASV");
		String response = readLine();
		if (!response.startsWith("227 ")) {
			throw new IOException("SimpleFTP could not request passive mode: " + response);
		}

		List<String> files = new ArrayList<String>();

		PassiveChannel pasvChannel = null;
		try {
			pasvChannel = PassiveChannel.connect(response);

			sendLine("LIST");
			final String listSent = readLine();
			if (!listSent.startsWith("150 ")) {
				throw new RuntimeException("Not sent");
			}

			String file;
			do {
				file = pasvChannel.readLine();
				if (null != file) {
					files.add(file);
				}
			} while (file != null);
		} finally {
			if (null != pasvChannel) {
				pasvChannel.close();
			}

		}

		final String dirSent = readLine();
		if (!dirSent.startsWith("226 ")) {
			throw new RuntimeException("Not sent");
		}

		return files;
	}

	public synchronized void createDirectory(String dir) throws Exception {
		if (dir == null) {
			throw new IllegalArgumentException("Null FTP directory name");
		}

		sendLine("MKD " + dir);

		String resp = readLine();
		if (!resp.startsWith("257 ")) {
			throw new IllegalAccessException(resp + "Can't create remote directory (" + resp + "): \"" + dir + "\"");
		}
	}

	public void removeDirectory(String dir) throws Exception {
		// Sanity checks
		if (dir == null) {
			throw new IllegalArgumentException("Null FTP directory name");
		}

		// Remove an existing remote directory
		sendLine("RMD " + dir);
		String resp = readLine();

		if (!resp.startsWith("250 ")) {
			throw new IllegalAccessException(resp + "Can't remove remote directory (" + resp + "): \"" + dir + "\"");
		}
	}

	/**
	 * Changes the working directory (like cd). Returns true if successful.
	 */
	public synchronized boolean cwd(String dir) throws IOException {
		sendLine("CWD " + dir);
		String response = readLine();
		return (response.startsWith("250 "));
	}

	/**
	 * Sends a file to be stored on the FTP server. Returns true if the file
	 * transfer was successful. The file is sent in passive mode to avoid NAT or
	 * firewall problems at the client end.
	 */
	public synchronized boolean stor(File file) throws IOException {
		if (file.isDirectory()) {
			throw new IOException("SimpleFTP cannot upload a directory.");
		}

		String filename = file.getName();
		return stor(new FileInputStream(file), filename);
	}

	/**
	 * Sends a file to be stored on the FTP server. Returns true if the file
	 * transfer was successful. The file is sent in passive mode to avoid NAT or
	 * firewall problems at the client end.
	 */
	public synchronized boolean stor(InputStream inputStream, String filename) throws IOException {

		BufferedInputStream input = new BufferedInputStream(inputStream);

		sendLine("PASV");
		String response = readLine();
		if (!response.startsWith("227 ")) {
			throw new IOException("SimpleFTP could not request passive mode: " + response);
		}
		PassiveChannel pasvChannel = PassiveChannel.connect(response);

		sendLine("STOR " + filename);

		response = readLine();
		if (!response.startsWith("125 ")) {
			//if (!response.startsWith("150 ")) {
			throw new IOException("SimpleFTP was not allowed to send the file: " + response);
		}

		BufferedOutputStream output = new BufferedOutputStream(pasvChannel.getSocket().getOutputStream());
		byte[] buffer = new byte[4096];
		int bytesRead = 0;
		while ((bytesRead = input.read(buffer)) != -1) {
			output.write(buffer, 0, bytesRead);
		}
		output.flush();
		output.close();
		input.close();

		response = readLine();
		return response.startsWith("226 ");
	}

	/**
	 * Enter binary mode for sending binary files.
	 */
	public synchronized boolean bin() throws IOException {
		sendLine("TYPE I");
		String response = readLine();
		return (response.startsWith("200 "));
	}

	/**
	 * Enter ASCII mode for sending text files. This is usually the default mode.
	 * Make sure you use binary mode if you are sending images or other binary
	 * data, as ASCII mode is likely to corrupt them.
	 */
	public synchronized boolean ascii() throws IOException {
		sendLine("TYPE A");
		String response = readLine();
		return (response.startsWith("200 "));
	}

	/**
	 * Sends a raw command to the FTP server.
	 */
	private void sendLine(String line) throws IOException {
		sendLine(socket, writer, line);
	}

	private String readLine() throws IOException {
		return readLine(reader);
	}

	static class PassiveChannel implements Closeable {

		private Socket socket;
		private BufferedReader reader;
		private BufferedWriter writer;

		public PassiveChannel(Socket socket, BufferedReader reader, BufferedWriter writer) {
			this.socket = socket;
			this.reader = reader;
			this.writer = writer;
		}

		static PassiveChannel connect(String passive) throws IOException {
			String ip = null;
			int port = -1;
			int opening = passive.indexOf('(');
			int closing = passive.indexOf(')', opening + 1);
			if (closing > 0) {
				String dataLink = passive.substring(opening + 1, closing);
				StringTokenizer tokenizer = new StringTokenizer(dataLink, ",");
				try {
					ip = tokenizer.nextToken() + "." + tokenizer.nextToken() + "." + tokenizer.nextToken() + "." + tokenizer.nextToken();
					port = Integer.parseInt(tokenizer.nextToken()) * 256 + Integer.parseInt(tokenizer.nextToken());
				} catch (Exception e) {
					throw new IOException("SimpleFTP received bad data link information: " + passive);
				}
			}
			return connect(ip, port);

		}

		static PassiveChannel connect(String host, int port) throws IOException {
			Socket socket = new Socket(host, port);
			BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			return new PassiveChannel(socket, reader, writer);
		}

		String readLine() throws IOException {
			return FTPClient.readLine(reader);
		}

		void write(String command) throws IOException {
			FTPClient.sendLine(socket, writer, command);
		}

		public Socket getSocket() {
			return socket;
		}

		public void close() {
			closeQuietly(socket);
		}

		private void closeQuietly(Closeable closeable) {
			if (null != closeable) {
				try {
					closeable.close();
				} catch (IOException e) {
					//do nothing
				}
			}
		}
	}

	private static void sendLine(Socket socket, Writer writer, String line) throws IOException {
		if (socket == null) {
			throw new IOException("SimpleFTP is not connected.");
		}
		try {
			writer.write(line + "\r\n");
			writer.flush();
			if (DEBUG) {
				log.info("> " + line);
			}
		} catch (IOException e) {
			socket = null;
			throw e;
		}
	}

	private static String readLine(BufferedReader reader) throws IOException {
		String line = reader.readLine();
		if (DEBUG) {
			log.info("< " + line);
		}
		return line;
	}
}