package by.ilokhin.elilink.tasks.helpers;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class MethodAddLettersToList {
	
	private static final Logger log = Logger.getLogger(MethodAddLettersToList.class.getName());
	
	public static void addLettersToList(WebDriver driver) throws InterruptedException {
		
		int count1 = driver.findElements(By.xpath(".//*[@id=':2']/div/div/div[4]/div[1]/div/table/tbody/tr")).size();
		
		List<List<String>> letters = new ArrayList<List<String>>(count1);
		for (int i = 0; i < count1; i++) {
			List<String> letter = new ArrayList<String>();
			letter.add("name: " + driver.findElement(By.xpath(
					".//*[@id=':2']/div/div/div[4]/div[1]/div/table/tbody/tr[" + (i + 1) + "]/td[4]/div[2]/span[1]"))
					.getText());
			letter.add(
					"topic: " + driver.findElement(By.xpath(".//*[@id=':2']/div/div/div[4]/div[1]/div/table/tbody/tr["
							+ (i + 1) + "]/td[6]/div/div/div[2]/span[1]")).getText());
			letter.add("text: " + driver.findElement(By.xpath(".//*[@id=':2']/div/div/div[4]/div[1]/div/table/tbody/tr["
					+ (i + 1) + "]/td[6]/div/div/div[2]/span[2]")).getText());
			letter.add("date: " + driver
					.findElement(By.xpath(
							".//*[@id=':2']/div/div/div[4]/div[1]/div/table/tbody/tr[" + (i + 1) + "]/td[8]/span"))
					.getText());
			letters.add(letter);
		}
		log.info(letters);
		
	}


}
